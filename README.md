# BuildTF
`BuildTF` provides examples of all the computation reported in Aydin et al. (submitted).

- [Overview](#overview)
- [System Requirements](#system-requirements)
- [Installation Guide](#installation-guide)
- [Documentation & Demo](#documentation-demo)

## Overview
`BuildTF`aims to help you better understand the transfer functions we describe in our article, and play with some of the parameters we had to deal with. It does not provide every option we tried within our study. 
This program only works with the HDF5 files provided by the authors (see [Installation Guide](#installation-guide)). A full and complete version will be released later.  


## System Requirements
### Hardware Requirements
`BuildTF`requires only a standard computer with enough RAM to support the in-memory operations.
### Software Requirements
This software has been compiled on Windows 10 Pro (v 18362.295) but should run on any of the Windows 10 or 7 operating systems.
It's been developed using Matlab 2018a and the following toolboxes :
+ MATLAB 
+ Global Optimization Toolbox
+ Signal Processing Toolbox
+ Optimization Toolbox
+ Curve Fitting Toolbox

Depending on your Matlab installation, see [Installation Guide](#installation-guide) below.
You may need a HDF5 file Viewer, downloadable on the HDF Group website [here](https://www.hdfgroup.org/downloads/hdfview).

## Installation Guide
The software is packaged to be used without having Matlab installed on your computer. 

+ Download here the HDF5 data files (too heavy to be be on GitLab) : [http://doi.org/10.5281/zenodo.3381353](http://doi.org/10.5281/zenodo.3381353)
+ Go to the [GitLab repository](https://gitlab.com/AliK_A/buildtf) 
+ Download :
  + `BuildTF.zip`, if you **have the Matlab Runtime environnement** on your computer, and unzip it. 
  + `BuildTF_WebInstaller.exe`, if you **do not have the Matlab Runtime environnement** and run the file to install.
+ Run the file `BuildTF.exe`and have fun !

Don't forget to install a HDF5 Viewer from [here](https://www.hdfgroup.org/downloads/hdfview).

## Documentation & Demo
The *How-To* is in a separate file : [HOWTO.md](https://gitlab.com/AliK_A/buildtf/blob/master/HOWTO.md). 



