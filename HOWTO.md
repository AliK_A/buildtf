# HowTo : Documentation & Demo

![Full view of the window](Images/FullWindow.JPG "image_tooltip")

**From HDF5 **: Path to the HDF5 file containing the data to start from for the computation (**Browse** button to search with the OS default selector).

**To HDF5 **: Path to the HDF5 file containing the data to end with for the computation (**Browse** button to search with the OS default selector - **Same** checkbox to set it automatically as **From HDF5**).

**Path From/To** : Paths inside the HDF5 files to access data. There is not (yet) any internal HDF5 tree visualizer, I suggest you download HDF View if you haven’t done yet ([https://www.hdfgroup.org/downloads/hdfview/](https://www.hdfgroup.org/downloads/hdfview/)). 

The usual paths we have for the files we are providing are:

/DeltaOverBSL/RBC/ET_200mV_5sec/avg

/DeltaOverBSL/RBC/ET_200mV_2sec/avg

/DeltaOverBSL/RBC/ET_200mV_1sec/avg

/DeltaOverBSL/RBC/ET_200mV_120msec/avg

/DeltaOverBSL/RBC/ET_1p2V_5sec/avg      and so on..

**Settings**

	**Range from (s) … to (s) …**: Time interval to use on the data to compute the TF on (we usually use 5 to 27s for 30s acquisition, and 5 to 57 for 60s because fUS timecourses starts often by a decreasing signal)

	**Duration (s)**: Duration of the TF you want to compute

	**Smoothing dT (ms)**: Smoothing applied using a spline algorithm in Matlab to the *From* data before computing the TF

	**Starting params : **Upper and lower bounds are 10 and 0.001 for each of them. Reminder for the function below. 


![TF Function Model](Images/TF_Equation "image_tooltip")


**Load & Display Data**
This button displays the datasets chosen, without any treatment. No TF in the panel yet.

![Window with data](Images/FullWindow_WData.JPG "image_tooltip")

Here we chose the XV4 mouse and thus put `XV4.h5`as filenames for both `From HDF`and `To HDF5`fields.

**Compute TF**
Compute the TF using the settings you set. *From* timecourse is now smoothed, and the prediction is plotted against the experimental data in the *To* plot (*To* timecourse is not treated in any way). Note that the time range has been used also.

![Computed Prediction](Images/Prediction.JPG "image_tooltip")

The **TF** panel plots the TF, the optimized parameters, and the residual value, from the subtraction of the the real RBC velocity timecourse and the prediction.
![Computed TF](Images/TF.JPG "image_tooltip")

** Note that doing exactly the same thing you may find a whole other TF and Prediction due to the randomness of a single run of the simulated annealing algorithm.** More runs are required, each time starting from the last set of parameters, to get to the right ones. 

